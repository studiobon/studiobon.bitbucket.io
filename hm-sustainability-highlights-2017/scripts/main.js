// Video hack because cq
function videoSrc() {
  $("video").each(function() {
    $this = $(this);
    $this.attr("src", $this.data("src"));
  });
}

$(function() {
  // Delay waypoints and video if editor mode
  if (typeof CQ != "undefined" && CQ.WCM.getMode() === "edit") {
    var wto = setTimeout(function() {
      videoSrc();
    }, 5000);
  } else {
    // Not editor, so fire up slick; waypoints and video immediately
    videoSrc();
  }

  $(".cards").masonry({
    itemSelector: ".card",
    gutter: ".gutter-sizer",
    percentPosition: true
  });

  $(".card img").on("load", function() {
    $(".cards").masonry("layout");
  });

  if (typeof $.subscribe == "function") {
    $.subscribe("video/onready", function() {
      $(".cards").masonry("layout");
    });
  }

  // Overlay logic
  var overlayController = {
    open: function(target) {
      if ($(target).hasClass("campaign-overlay")) {
        $(target)
          .scrollTop(0)
          .addClass("campaign-overlay--is-open");
        $("body").addClass("is-scroll-disabled");
        history.pushState("", document.title, target);
      }
    },
    close: function(target) {
      $(".campaign-overlay--is-open").removeClass("campaign-overlay--is-open");
      $("body").removeClass("is-scroll-disabled");
      history.pushState(
        "",
        document.title,
        window.location.pathname + window.location.search
      );
    }
  };

  $(".campaign-overlay-toggle").on("click", function(e) {
    var target = $(this).data("href");
    e.preventDefault();
    overlayController.open(target);
  });

  $(".campaign-overlay__close-btn").on("click", function(e) {
    overlayController.close();
  });

  // Close overlay when ESC is pressed
  $(document).keyup(function(e) {
    if (e.keyCode === 27 && $(".campaign-overlay--is-open").length > 0)
      overlayController.close();
  });

  // Open overlay if url target hash is present
  if (window.location.hash !== "" && window.location.hash.indexOf("q=") < 0) {
    overlayController.open(window.location.hash);
  }

  // Open external links in new window
  // sigh
  $(".campaign-overlay a")
    .filter(function() {
      return this.hostname && this.hostname !== location.hostname;
    })
    .attr("target", "_blank");
});
